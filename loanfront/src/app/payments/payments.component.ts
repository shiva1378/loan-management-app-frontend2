import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Payments } from '../payments';
import { User } from '../user';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent {
  addForm!: FormGroup
  submitted=false;
//loan:Loan=new Loan()
 // constructor(private service:UserServiceService,private _router:Router){}
 constructor(private issueService:UserServiceService,private fb:FormBuilder,private router:Router,private http:HttpClient)
 {
  this.addForm=this.fb.group({
    totalAmt:this.fb.control("",Validators.required),
    duration:this.fb.control("",Validators.required),
    monthlyEMI:this.fb.control("",Validators.required),
    id:this.fb.control("",Validators.required)
  })
 }
 onSubmit()
{
  const payment=new Payments();
  payment.customer=new User();
  payment.totalAmt=this.addForm.controls['totalAmt'].value;
  payment.duration=this.addForm.controls['duration'].value;
  payment.monthlyEMI=this.addForm.controls['monthlyEMI'].value;
  payment.customer.id=this.addForm.controls['id'].value;
  console.log(payment);
  
  this.issueService.createPayment(payment).subscribe((data:any)=>{
    console.log(data)   },
  error=>console.log(error));
  // if (this.submitted){
  alert("Payment successfully done");
  this.router.navigate(['mypayments']);
  // }
}
}
