

import { User } from "./user";


export class Loan {
    loanId!:number;
   loanAmt!:number;
    loanType:String="";
    duration!:number;
    interest!:number;
    monthlyEMI!:number;
    customer!:User;
}
